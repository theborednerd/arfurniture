﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceableButtonData : MonoBehaviour
{
    private int _index;

    public Button button;

    public void AddOnClick(int index, System.Action<int> callBack)
    {
        _index = index;
        button.onClick.AddListener(delegate { callBack(_index); });
    }
}
