﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapeOptionSelector : MonoBehaviour
{
    public TMPro.TextMeshProUGUI lblTitle;
    public TMPro.TextMeshProUGUI lblOption;

    private string[] _optionNames;
    private int _optionIndex;
    private Placeable _placeable;

    public Slider slider;

    private bool inited = false;

    public void Init(Placeable placeable, string title, int optionIndex, GameObject[] options)
    {
        _placeable = placeable;
        _optionIndex = optionIndex;

        //Could get title from placeable direct but this way i can overwrite it if i want
        lblTitle.text = title;
        
        _optionNames = new string[options.Length];
        for (int i=0; i< options.Length; i++)
        {
            _optionNames[i] = options[i].name;
        }

        slider.value = placeable.shapeOptions[optionIndex].currentObject;
        lblOption.text = options[(int)slider.value].name;

        slider.maxValue = options.Length - 1;

        

        inited = true;
    }

    public void Increment()
    {
        if (inited)
        {
            if (slider.value + 1 > slider.maxValue)
                slider.value = 0;
            else
                slider.value += 1;

            lblOption.text = _optionNames[(int)slider.value];
        }
    }

    public void Decrement()
    {
        if (inited)
        {
            if (slider.value - 1 < 0)
                slider.value = slider.maxValue;
            else
                slider.value -= 1;

            lblOption.text = _optionNames[(int)slider.value];
        }
    }

    public void OnValueChanged(float val)
    {
        if (inited)
        {
            _placeable.ChangeShape(_optionIndex, (int)val);
        }
    }
}
