﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placeable : MonoBehaviour
{
    [Serializable]
    public struct ShapeOption
    {
        public string optionName;
        public GameObject[] optionObjects;
        [HideInInspector]
        public float currentObject;
    }

    [Serializable]
    public struct ColourOption
    {
        public string optionName;
        [Tooltip("All objects in this lisht will have same colour applied to them")]
        public GameObject[] optionObjects;
        [HideInInspector]
        public Material[] optionsMats;
    }

    private Outline _highlighter;
    public GameObject mover;

    private Rigidbody _rigidbody;
    private Collider[] _colliders;

    public float backToKinematicDelay;
    private float _backToKinematicWaited;

    public Texture thumbnail;

    public bool canPlaceOnVert = false;
    public bool canPlaceOnHori = true;

    [HideInInspector]
    public bool currentlyOnWall = false;
    [HideInInspector]
    public bool currentlyOnFloor = false;

    [SerializeField]
    public ShapeOption[] shapeOptions;

    [SerializeField]
    public ColourOption[] colourOptions;

    // Start is called before the first frame update
    void Awake()
    {
        _highlighter = GetComponentInChildren<Outline>();
        _highlighter.enabled = false;

        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.isKinematic = true;

        _colliders = GetComponentsInChildren<Collider>();

        //Turn off all options but the first one
        for(int a =0; a< shapeOptions.Length; a++)
        {
            for (int b=1; b< shapeOptions[a].optionObjects.Length; b++)
            {
                shapeOptions[a].optionObjects[b].SetActive(false);
            }

            shapeOptions[a].currentObject = 0;
        }

        List<Material> tempMatList = new List<Material>();
        for (int a = 0; a < colourOptions.Length; a++)
        {
            for (int b = 0; b < colourOptions[a].optionObjects.Length; b++)
            {
                Renderer[] tempRenderers = colourOptions[a].optionObjects[b].GetComponentsInChildren<Renderer>();

                for (int c = 0; c < tempRenderers.Length; c++)
                {
                    tempMatList.AddRange(tempRenderers[c].materials);
                }
            }

            colourOptions[a].optionsMats = tempMatList.ToArray();
            tempMatList.Clear();

        }
    }

    private void Update()
    {
        if (!_rigidbody.isKinematic)
        {
            _backToKinematicWaited += Time.deltaTime;

            if(_backToKinematicWaited >= backToKinematicDelay)
            {
                _backToKinematicWaited = 0f;
                _rigidbody.isKinematic = true;
            }
        }
    }

    public void SetHighlight(bool higlight)
    {
        _highlighter.enabled = higlight;
    }

    public void Move(Vector3 pos)
    {
        _rigidbody.isKinematic = false;
        _backToKinematicWaited = 0f;

        mover.transform.position = pos;
    }

    public void SetCollidersEnabled(bool enabled)
    {

        for(int i=0; i< _colliders.Length; i++)
        {
            _colliders[i].enabled = enabled;
        }
    }

    public void ChangeShape(int optionIndex, int optionObjectIndex)
    {
        if(optionIndex >= 0 && optionIndex < shapeOptions.Length)
        {
            if(optionObjectIndex >=0 && optionObjectIndex < shapeOptions[optionIndex].optionObjects.Length)
            {
                for (int i = 0; i < shapeOptions[optionIndex].optionObjects.Length; i++)
                {
                    shapeOptions[optionIndex].optionObjects[i].SetActive(false);
                }

                shapeOptions[optionIndex].optionObjects[optionObjectIndex].SetActive(true);

                shapeOptions[optionIndex].currentObject = optionObjectIndex;
            }
        }
    }

}
