﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class SceneController : MonoBehaviour
{
    public bool showTrackingplanes = false;
    public Transform arWorld;

    public ARRaycastManager raycastManager;

    static List<ARRaycastHit> _raycastHits = new List<ARRaycastHit>();

    private Placeable _activeObject;
    private bool _activeObjectMoving = false;
    private Placeable _highlightedObject;

    public Button btnEdit;
    public Slider sliRotate;
    public GameObject editPanel;
    public RectTransform sidePanel;
    public Button btnExpandSide;
    public Button btnCloseSide;
    public GameObject WarningPanel;

    public RectTransform placeablesListParent;
    public GameObject placeablesListButtonPrefab;

    private bool _initillyPlaced = false;

    [Space]
    public GameObject[] placabels;


    // Start is called before the first frame update
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.targetFrameRate = 60;
        Application.RequestUserAuthorization(UserAuthorization.WebCam);

#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
        }
#endif

        HideShowSidePanel(false);

        for (int i=0; i < placabels.Length; i++)
        {
            GameObject temp = Instantiate(placeablesListButtonPrefab, placeablesListParent);
            temp.name = placabels[i].name;

            temp.GetComponentInChildren<RawImage>().texture = placabels[i].GetComponent<Placeable>().thumbnail;
            temp.GetComponent<PlaceableButtonData>().AddOnClick(i, AddNewPlaceable);
        }
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit[] hits;

        if (_activeObject != null )
        {
            if (_activeObjectMoving || !_initillyPlaced)
            {
                //Disable collider on active object so it doesn't get Raycasted
                _activeObject.SetCollidersEnabled(false);
                hits = Physics.RaycastAll(Camera.main.transform.position, Camera.main.transform.forward, 10000.0F);

                if (hits.Length > 0)
                {
                    RaycastHit hit = FindClosestHit(hits);

                    float dot = Vector3.Dot(hit.normal, Vector3.up);

                    _activeObject.currentlyOnWall = false;
                    _activeObject.currentlyOnFloor = false;

                    if ( (dot > 0.8f && _activeObject.canPlaceOnHori) )
                    {
                        _activeObject.currentlyOnFloor = true;
                    }

                    if ((dot < 0.2f && dot > -0.2f) && _activeObject.canPlaceOnVert)
                    {
                        _activeObject.currentlyOnWall = true;
                    }

                    if(_activeObject.currentlyOnFloor || _activeObject.currentlyOnWall)
                    {
                        _activeObject.Move(hit.point);
                        _initillyPlaced = true;

                        if (hit.collider.gameObject.layer == 8)
                        {
                            _activeObject.transform.parent = hit.collider.gameObject.GetComponentInParent<Placeable>().transform;
                        }
                        else
                        {
                            _activeObject.transform.parent = arWorld;
                        }

                    }

                    if (_activeObject.currentlyOnWall)
                    {
                        _activeObject.transform.rotation = Quaternion.LookRotation(-hit.normal);
                    }

                }

                _activeObject.SetCollidersEnabled(true);
            }

            if (_activeObject.currentlyOnWall)
            {
                _activeObject.transform.eulerAngles = new Vector3(_activeObject.transform.eulerAngles.x, 
                                                                _activeObject.transform.eulerAngles.y, 
                                                                sliRotate.value);
            }
            else
            {
                _activeObject.transform.eulerAngles = new Vector3(_activeObject.transform.eulerAngles.x, 
                                                                sliRotate.value, 
                                                                _activeObject.transform.eulerAngles.z);
            }
        }
        else
        {
            if (_highlightedObject != null)
            {
                _highlightedObject.SetHighlight(false);
            }
            _highlightedObject = null;

            hits = Physics.RaycastAll(Camera.main.transform.position, Camera.main.transform.forward, 10000.0F, ~8);

            if (hits.Length > 0)
            {
                
                _highlightedObject = FindClosestHit(hits).collider.gameObject.GetComponentInParent<Placeable>();

                if (_highlightedObject != null)
                {
                    _highlightedObject.SetHighlight(true);
                } 
            }

        }


        CheckTrackingState();
    }

    public RaycastHit FindClosestHit(RaycastHit[] hits)
    {
        RaycastHit closesHit = hits[0];
        for (int i = 1; i < hits.Length; i++)
        {
            if (hits[i].distance <= closesHit.distance) {
                    closesHit = hits[i];
            }
        }

        return closesHit;
    }


    public void OnEditClicked()
    {
        if (_highlightedObject != null) {
            _activeObject = _highlightedObject;
            _highlightedObject = null;
            btnEdit.gameObject.SetActive(false);
            editPanel.SetActive(true);

            editPanel.GetComponent<EditPanel>().Init(_activeObject);

            showTrackingplanes = true;

            if (_activeObject.currentlyOnWall)
            {
                sliRotate.value = _activeObject.transform.eulerAngles.z;
            }
            else
            {
                sliRotate.value = _activeObject.transform.eulerAngles.y;
            }
            
        }

        HideShowSidePanel(false);
    }

    public void OnMovedPressed()
    {
        _activeObjectMoving = true;
    }

    public void OnMoveRealesed()
    {
        _activeObjectMoving = false;
    }

    public void OnEditDone()
    {
        btnEdit.gameObject.SetActive(true);
        editPanel.SetActive(false);
        showTrackingplanes = false;

        _activeObjectMoving = false;

        _activeObject.SetHighlight(false);
        _activeObject = null;
        
    }

    public void AddNewPlaceable(int index)
    {
        if(_activeObject != null)
        {
            OnEditDone();
        }

        _highlightedObject = Instantiate(placabels[index], arWorld).GetComponent<Placeable>();
        _highlightedObject.SetHighlight(true);

        OnEditClicked();
        _initillyPlaced = false;
    }

    public void HideShowSidePanel(bool show)
    {
        if (show)
        {
            btnExpandSide.gameObject.SetActive(false);
            btnCloseSide.gameObject.SetActive(true);

            LeanTween.move(sidePanel, new Vector2(100, 0), 0.250f);
        }
        else
        {
            btnExpandSide.gameObject.SetActive(true);
            btnCloseSide.gameObject.SetActive(false);

            LeanTween.move(sidePanel, new Vector2(-100, 0), 0.250f);
        }
    }

    public void CheckTrackingState()
    {

        if (ARSession.state == ARSessionState.SessionTracking || Application.isEditor)
        {
            if (WarningPanel.activeSelf)
                WarningPanel.SetActive(false);
        }
        else
        {
            if (!WarningPanel.activeSelf)
                WarningPanel.SetActive(true);
        }
    }

    public void RemovePlaceable()
    {
        if (_activeObject != null)
        {
            btnEdit.gameObject.SetActive(true);
            editPanel.SetActive(false);
            showTrackingplanes = false;

            _activeObjectMoving = false;

            _activeObject.SetHighlight(false);

            Destroy(_activeObject.gameObject);

            _activeObject = null;
        }
    }
}
