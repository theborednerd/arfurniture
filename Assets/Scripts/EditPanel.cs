﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditPanel : MonoBehaviour
{
    public GameObject shapeOptionSelectorPerfab;
    public GameObject colourSelectorPerfab;
    public Transform optionsParent;

    private GameObject[] _shapeOtions;
    private GameObject[] _colourPickers;

    public void Init(Placeable placeable)
    {
        ClearOld();

        _shapeOtions = new GameObject[placeable.shapeOptions.Length];
        

        for (int i=0; i< _shapeOtions.Length; i++)
        {
            _shapeOtions[i] = Instantiate(shapeOptionSelectorPerfab, optionsParent);
            _shapeOtions[i].GetComponent<ShapeOptionSelector>().Init(placeable,
                                                                        placeable.shapeOptions[i].optionName,
                                                                        i, placeable.shapeOptions[i].optionObjects);
        }

        _colourPickers = new GameObject[placeable.colourOptions.Length];

        for (int i = 0; i < _colourPickers.Length; i++)
        {
            _colourPickers[i] = Instantiate(colourSelectorPerfab, optionsParent);

            _colourPickers[i].GetComponent<ColourPicker>().Init(placeable.colourOptions[i].optionName,
                                                                placeable.colourOptions[i].optionsMats);
        }

    }

    private void OnDisable()
    {
        ClearOld();
    }

    public void ClearOld()
    {
        if (_shapeOtions != null)
        {
            for (int i = 0; i < _shapeOtions.Length; i++)
            {
                Destroy(_shapeOtions[i]);
            }

            _shapeOtions = null;
        }

        if (_colourPickers != null)
        {
            for (int i = 0; i < _colourPickers.Length; i++)
            {
                Destroy(_colourPickers[i]);
            }

            _colourPickers = null;
        }
    }
}
