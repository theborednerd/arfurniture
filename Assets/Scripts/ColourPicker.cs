﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColourPicker : MonoBehaviour
{
    public TMPro.TextMeshProUGUI lblTitle;
    public Slider sliH;
    public Slider sliS;
    public Image sliSImage;
    public Slider sliV;
    public Image sliVImage;

    private Material[] matToChange;

    public void Init(string title, Material[] mat)
    {
        matToChange = mat;
        lblTitle.text = title;
        Color tempColour = Color.HSVToRGB(sliH.value, sliS.value, sliV.value);

        if (matToChange != null)
        {
            float h, s, v;
            Color.RGBToHSV(mat[0].color, out h, out s, out v);

            sliH.value = h;
            sliS.value = s;
            sliV.value = v;
        }
    }


    public void OnColourChanged()
    {
        Color tempColour = Color.HSVToRGB(sliH.value, sliS.value, sliV.value);
        sliSImage.color = tempColour;
        sliVImage.color = tempColour;

        if (matToChange != null)
        {
            for(int i=0; i< matToChange.Length; i++)
            matToChange[i].color = tempColour;
        }
    }
}
