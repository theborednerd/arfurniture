﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class ThumbnailGen : EditorWindow
{
    public GameObject[] objects;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/ThumbnailGen")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        ThumbnailGen window = (ThumbnailGen)EditorWindow.GetWindow(typeof(ThumbnailGen));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Objects", EditorStyles.boldLabel);

        ScriptableObject scriptableObj = this;
        SerializedObject serialObj = new SerializedObject(scriptableObj);
        SerializedProperty serialProp = serialObj.FindProperty("objects");

        EditorGUILayout.PropertyField(serialProp, true);
        serialObj.ApplyModifiedProperties();

        if (GUILayout.Button("GoBabyGo!"))
        {
            for(int i=0; i < objects.Length; i++)
            {
                Texture2D temp = AssetPreview.GetAssetPreview(objects[i]);

                byte[] bytes = temp.EncodeToPNG();
                var dirPath = Application.dataPath + "/Textures/";
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                File.WriteAllBytes(dirPath + objects[i].name + ".png", bytes);
            }
        }

    }
}